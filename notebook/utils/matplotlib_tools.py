import numpy as np

def draw_box(figure, signal_values, box_x=0., box_width=0.8333, color='none', box_color='k', linewidth=2, alpha=1, outlier_size=20, outlier_alpha=0.33):
    """
    """

    signal_time_percentiles = {}
    for percentile in [10, 25, 50, 75, 90]:
        signal_time_percentiles[percentile] = np.nanpercentile(signal_values, percentile)
        signal_time_mean = np.nanmean(signal_values)

    figure.gca().fill_between([box_x - box_width, box_x + box_width], [signal_time_percentiles[75], signal_time_percentiles[75]], [signal_time_percentiles[25], signal_time_percentiles[25]],
                              facecolor=color, alpha=alpha)
    figure.gca().plot([box_x - box_width, box_x + box_width], [signal_time_percentiles[50], signal_time_percentiles[50]], color=box_color, linewidth=1.5 * linewidth, alpha=alpha)
    figure.gca().plot([box_x - box_width, box_x + box_width], [signal_time_percentiles[25], signal_time_percentiles[25]], color=box_color, linewidth=linewidth / 2., alpha=alpha)
    figure.gca().plot([box_x - box_width, box_x + box_width], [signal_time_percentiles[75], signal_time_percentiles[75]], color=box_color, linewidth=linewidth / 2., alpha=alpha)
    figure.gca().plot([box_x - box_width, box_x - box_width], [signal_time_percentiles[25], signal_time_percentiles[75]], color=box_color, linewidth=linewidth / 2., alpha=alpha)
    figure.gca().plot([box_x + box_width, box_x + box_width], [signal_time_percentiles[25], signal_time_percentiles[75]], color=box_color, linewidth=linewidth / 2., alpha=alpha)
    figure.gca().plot([box_x, box_x], [signal_time_percentiles[25], signal_time_percentiles[10]], color=box_color, linewidth=linewidth / 2., linestyle='dashed', alpha=alpha)
    figure.gca().plot([box_x, box_x], [signal_time_percentiles[75], signal_time_percentiles[90]], color=box_color, linewidth=linewidth / 2., linestyle='dashed', alpha=alpha)
    figure.gca().plot([box_x - box_width / 5., box_x + box_width / 5.], [signal_time_percentiles[10], signal_time_percentiles[10]], color=box_color, linewidth=linewidth / 2., alpha=alpha)
    figure.gca().plot([box_x - box_width / 5., box_x + box_width / 5.], [signal_time_percentiles[90], signal_time_percentiles[90]], color=box_color, linewidth=linewidth / 2., alpha=alpha)

    outliers = [d for d in signal_values if (d < signal_time_percentiles[10]) or (d > signal_time_percentiles[90])]
    outliers_x = [box_x for d in outliers]
    figure.gca().scatter(outliers_x, outliers, s=outlier_size, facecolor='none', edgecolor='k', linewidth=1, alpha=outlier_alpha)

