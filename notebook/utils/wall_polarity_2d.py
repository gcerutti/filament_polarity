import logging

import numpy as np
import scipy.ndimage as nd
import pandas as pd

from scipy.optimize import curve_fit
from scipy.cluster.vq import vq
from scipy.stats import norm as normal_distribution
from scipy.stats import wilcoxon


def normal_function(x, amplitude=1, loc=0, scale=1, offset=0):
    return offset+amplitude*normal_distribution.pdf(x,loc=loc,scale=scale)


def quantify_wall_signals(wall_topomesh, cell_labels, img_dict, membrane_channel='Calco', channel_names=['SOK4'], wall_distance=1.5, wall_sigma=5., line_sigma=1., exclude_contours=False, distance_profile=False, distance_bin=0.1):
    
    left_label, right_label = cell_labels
    wall_label = (left_label, right_label)
    membrane_img = img_dict[membrane_channel]

    wall_vertex_data = {}
    for channel_name in channel_names:
        wall_vertex_data[channel_name] = dict([(v,np.nan) for v in wall_topomesh.wisps(0)])
        wall_vertex_data[channel_name+"_left"] = dict([(v,np.nan) for v in wall_topomesh.wisps(0)])
        wall_vertex_data[channel_name+"_right"] = dict([(v,np.nan) for v in wall_topomesh.wisps(0)])

    wall_data = {}
    wall_data['label'] = [wall_label]
    wall_data['left_label'] = [left_label]
    wall_data['right_label'] = [right_label]
    for channel_name in channel_names:
        wall_data[channel_name] = [np.nan]
        wall_data[channel_name+"_left"] = [np.nan]
        wall_data[channel_name+"_right"] = [np.nan]
        wall_data[channel_name+"_polarity"] = [np.nan]
                
    wall_bbox = []
    wall_bbox += [np.min([wall_topomesh.wisp_property('barycenter',0).values()[:, :2]+side*wall_sigma*wall_topomesh.wisp_property('normal',0).values()[:, :2] for side in [-1,1]],axis=(0,1))]
    wall_bbox += [np.max([wall_topomesh.wisp_property('barycenter',0).values()[:, :2]+side*wall_sigma*wall_topomesh.wisp_property('normal',0).values()[:, :2] for side in [-1,1]],axis=(0,1))]

    wall_bbox = np.sort(np.transpose(wall_bbox)/np.array(membrane_img.voxelsize)[:,np.newaxis])
    wall_bbox[:,0] = np.minimum(np.maximum(0,np.floor(wall_bbox[:,0])),np.array(membrane_img.shape)-1)
    wall_bbox[:,1] = np.minimum(np.maximum(0,np.ceil(wall_bbox[:,1])),np.array(membrane_img.shape)-1)
    
    wall_neighborhood_coords = np.mgrid[wall_bbox[0, 0]:wall_bbox[0, 1] + 1, wall_bbox[1, 0]:wall_bbox[1, 1] + 1]
    wall_neighborhood_coords = np.concatenate(np.transpose(wall_neighborhood_coords, (1, 2, 0)))
    wall_neighborhood_coords = np.unique(wall_neighborhood_coords, axis=0).astype(int)

    wall_neighborhood_points = (wall_neighborhood_coords * np.array(membrane_img.voxelsize))
    wall_neighborhood_coords = tuple(np.transpose(wall_neighborhood_coords))
    wall_neighborhood_membrane = membrane_img[wall_neighborhood_coords]

    wall_neighborhood_signal = {}
    for channel_name in channel_names:
        signal_img = img_dict[channel_name]
        wall_neighborhood_signal[channel_name] = signal_img[wall_neighborhood_coords]

    wall_vertices = np.array(list(wall_topomesh.wisps(0)))
    if exclude_contours:
        wall_vertices = [v for v in wall_vertices if wall_topomesh.nb_regions(0, v)>1]

    if len(wall_vertices) > 0:
        wall_points = wall_topomesh.wisp_property('barycenter', 0).values(wall_vertices)[:, :2]
        wall_normal_vectors = wall_topomesh.wisp_property('normal', 0).values(wall_vertices)[:, :2]

        wall_neighborhood_vectors = wall_neighborhood_points[np.newaxis, :] - wall_points[:, np.newaxis]
        wall_neighborhood_dot_products = np.einsum("...ij,...ij->...i", wall_neighborhood_vectors, wall_normal_vectors[:, np.newaxis])

        wall_neighborhood_projected_points = wall_points[:, np.newaxis] + wall_neighborhood_dot_products[:, :, np.newaxis] * wall_normal_vectors[:, np.newaxis]
        wall_neighborhood_projected_distances = np.linalg.norm(wall_neighborhood_projected_points - wall_neighborhood_points[np.newaxis, :], axis=2)

        membrane_shifts = []
        signal_vertices = dict((c, []) for c in channel_names)
        signal_left_intensities = dict((c, []) for c in channel_names)
        signal_right_intensities = dict((c, []) for c in channel_names)

        logging.debug("    --> " + str(len(wall_neighborhood_dot_products)) + " lines to consider")

        if distance_profile:
            distance_bins = np.linspace(-wall_sigma/2, wall_sigma/2, int(np.round(wall_sigma/distance_bin))+1)

        for i_line, (wall_distances, line_distances, wall_vertex) in enumerate(zip(wall_neighborhood_dot_products, wall_neighborhood_projected_distances, wall_vertices)):
            line_weights = (np.exp(-np.power(line_distances, 2) / np.power(line_sigma, 2))) * (np.abs(wall_distances) < wall_sigma)

            # p0 is the initial guess for the fitting coefficients (A, mu and sigma above)
            p0 = [1., 0., 1., 0.]

            try:

                line_weights = (line_weights > 0.1).astype(float)
                if not distance_profile:
                    membrane_p, membrane_cov = curve_fit(normal_function, wall_distances[line_weights > 0.1], wall_neighborhood_membrane[line_weights > 0.1], p0=p0, maxfev=int(1e3))
                    membrane_amplitude, membrane_loc, membrane_scale, membrane_offset = membrane_p
                    membrane_shifts += [membrane_loc]
                else:
                    loc_bins = np.linspace(-wall_sigma, wall_sigma, 101)
                    loc_bin = loc_bins[1] - loc_bins[0]
                    loc_bin_matching = vq(wall_distances, loc_bins)
                    loc_filter = (line_weights > 0.1) & (loc_bin_matching[1] <= loc_bin/2)
                    if (loc_filter.sum() > 0):
                        wall_loc_bins = loc_bins[loc_bin_matching[0][loc_filter]]

                        loc_bin_membrane_intensities = nd.mean(wall_neighborhood_membrane[loc_filter], wall_loc_bins, loc_bins)
                        membrane_loc = loc_bins[np.nanargmax(loc_bin_membrane_intensities)]
                    else:
                        raise ValueError(f"Not enough voxels around vertex!") 

            except Exception as e:
                print("vertex", wall_vertex, e)
            else:
                membrane_wall_distances = wall_distances - membrane_loc
                # normal left->right : left<=0, right>=0

                if not distance_profile:
                    left_filter = (line_weights > 0.1) & (membrane_wall_distances <= 0) & (np.abs(membrane_wall_distances) < wall_distance)
                    right_filter = (line_weights > 0.1) & (membrane_wall_distances >= 0) & (np.abs(membrane_wall_distances) < wall_distance)

                    if (right_filter.sum() > 0) and (left_filter.sum() > 0):

                        for i_c, channel_name in enumerate(channel_names):
                            left_signal = wall_neighborhood_signal[channel_name][left_filter].mean()
                            right_signal = wall_neighborhood_signal[channel_name][right_filter].mean()

                            signal_vertices[channel_name] += [wall_vertex]
                            signal_left_intensities[channel_name] += [left_signal]
                            signal_right_intensities[channel_name] += [right_signal]
                else:
                    distance_bin_matching = vq(membrane_wall_distances, distance_bins)
                    distance_filter = (line_weights > 0.1) & (distance_bin_matching[1] <= distance_bin/2)
                    if (distance_filter.sum() > 0):
                        wall_distance_bins = distance_bins[distance_bin_matching[0][distance_filter]]

                        for i_c, channel_name in enumerate(channel_names):
                            distance_bin_intensities = nd.mean(wall_neighborhood_signal[channel_name][distance_filter], wall_distance_bins, distance_bins)

                            signal_vertices[channel_name] += [wall_vertex]
                            signal_left_intensities[channel_name] += [dict(zip(distance_bins, distance_bin_intensities))]

        for i_c, channel_name in enumerate(channel_names):
            if len(signal_left_intensities[channel_name]) > 0:
                signal_vertices[channel_name] = np.array(signal_vertices[channel_name])
                if not distance_profile:
                    signal_left_intensities[channel_name] = np.array(signal_left_intensities[channel_name])
                    signal_right_intensities[channel_name] = np.array(signal_right_intensities[channel_name])
                    wall_vertex_data[f"{channel_name}_left"].update(dict(zip(signal_vertices[channel_name], signal_left_intensities[channel_name])))
                    wall_vertex_data[f"{channel_name}_right"].update(dict(zip(signal_vertices[channel_name], signal_right_intensities[channel_name])))
                    wall_vertex_data[channel_name].update(dict(zip(signal_vertices[channel_name], np.mean([signal_left_intensities[channel_name], signal_right_intensities[channel_name]], axis=0))))

                    wall_data[channel_name] = [np.nanmean([np.nanmedian(signal_left_intensities[channel_name]), np.nanmedian(signal_right_intensities[channel_name])])]

                    pvalue_thresholds = {"*": 0.1, "**": 0.05, "***": 0.01}

                    if len(signal_left_intensities) > 0:
                        if np.all(signal_left_intensities[channel_name] - signal_right_intensities[channel_name]==0):
                            signal_polarity = 0
                        else:
                            statistic, greater_pvalue = wilcoxon(signal_left_intensities[channel_name], signal_right_intensities[channel_name], alternative='greater')
                            statistic, less_pvalue = wilcoxon(signal_left_intensities[channel_name], signal_right_intensities[channel_name], alternative='less')

                            if greater_pvalue < pvalue_thresholds["*"] and less_pvalue > pvalue_thresholds["*"]:
                                if greater_pvalue < pvalue_thresholds["***"]:
                                    signal_polarity = 1
                                elif greater_pvalue < pvalue_thresholds["**"]:
                                    signal_polarity = 0.5
                                else:
                                    signal_polarity = 0.25
                            elif less_pvalue < pvalue_thresholds["*"] and greater_pvalue > pvalue_thresholds["*"]:
                                if less_pvalue < pvalue_thresholds["***"]:
                                    signal_polarity = -1
                                elif less_pvalue < pvalue_thresholds["**"]:
                                    signal_polarity = -0.5
                                else:
                                    signal_polarity = -0.25
                            else:
                                signal_polarity = 0

                            print(signal_polarity," <-> ", np.sign(np.nanmedian(signal_left_intensities[channel_name] - signal_right_intensities[channel_name])))

                        wall_data[channel_name+"_polarity"] = [signal_polarity]
                        wall_data[channel_name+"_left"] = [np.nanmedian(signal_left_intensities[channel_name])]
                        wall_data[channel_name+"_right"] = [np.nanmedian(signal_right_intensities[channel_name])]
                else:
                    signal_left_intensities[channel_name] = {b: np.array([s[b] for s in signal_left_intensities[channel_name]])
                                                             for b in signal_left_intensities[channel_name][0]}

                    wall_vertex_data[f"{channel_name}_profile"] = {b: dict(zip(signal_vertices[channel_name], s))
                                                                   for b,s in signal_left_intensities[channel_name].items()}
    return wall_vertex_data, wall_data
                    