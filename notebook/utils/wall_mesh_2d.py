import numpy as np

from cellcomplex.property_topomesh.analysis import compute_topomesh_property, compute_topomesh_vertex_property_from_edges
from cellcomplex.property_topomesh.topological_operations import topomesh_remove_boundary_vertex
from cellcomplex.property_topomesh.optimization import property_topomesh_vertices_deformation


def compute_wall_center(topomesh):
    compute_topomesh_property(topomesh, 'barycenter', 1)
    compute_topomesh_property(topomesh, 'length', 1)
    wall_edges = np.array(list(topomesh.wisps(1)))
    wall_edge_lengths = topomesh.wisp_property('length', 1).values(wall_edges)
    wall_edge_centers = topomesh.wisp_property('barycenter', 1).values(wall_edges)
    
    wall_center = np.sum(wall_edge_lengths[:,np.newaxis]*wall_edge_centers, axis=0)/np.sum(wall_edge_lengths)
    return wall_center


def compute_wall_normal(topomesh):
    compute_topomesh_property(topomesh, 'length', 1)
    wall_edges = np.array(list(topomesh.wisps(1)))
    wall_edge_lengths = topomesh.wisp_property('length', 1).values(wall_edges)
    wall_edge_normals = topomesh.wisp_property('normal', 1).values(wall_edges)
    
    wall_normal = np.sum(wall_edge_lengths[:,np.newaxis]*wall_edge_normals, axis=0)/np.sum(wall_edge_lengths)
    wall_normal /= np.linalg.norm(wall_normal)
    return wall_normal


def wall_mesh_optimization(topomesh, target_edge_length=1.):
    border_vertices = np.array([v for v in topomesh.wisps(0) if topomesh.nb_regions(0, v) == 1])
    border_points = topomesh.wisp_property('barycenter', 0).values(border_vertices)
    
    property_topomesh_vertices_deformation(topomesh, 
                                           omega_forces={'laplacian_smoothing':0.1}, 
                                           sigma_deformation=target_edge_length/10., 
                                           iterations=10)
    vertex_positions = topomesh.wisp_property('barycenter', 0).to_dict()
    vertex_positions.update(dict(zip(border_vertices, border_points)))
    topomesh.update_wisp_property('barycenter', 0, vertex_positions)
    
    compute_topomesh_property(topomesh, 'vertices', 1)
    compute_topomesh_property(topomesh, 'length', 1)
    print(f"Average edge length = {topomesh.wisp_property('length', 1).values().mean()}")
    compute_topomesh_vertex_property_from_edges(topomesh, 'length', weighting='uniform')
    
    inner_vertices = np.array([v for v in topomesh.wisps(0) if v not in border_vertices])

    if len(inner_vertices) > 0:
        inner_vertex_lengths = topomesh.wisp_property('length', 0).values(inner_vertices)
        inner_vertex_length_sorting = np.argsort(inner_vertex_lengths)

        sorted_inner_vertices = inner_vertices[inner_vertex_length_sorting]
        sorted_inner_vertex_lengths = inner_vertex_lengths[inner_vertex_length_sorting]

        vertices_to_remove = sorted_inner_vertices[sorted_inner_vertex_lengths < target_edge_length]
    else:
        vertices_to_remove = []
    
    while len(vertices_to_remove) > 0:
        affected_vertices = set()
        removed_vertices = 0
        for v in vertices_to_remove:
            if not v in affected_vertices:
                affected_vertices |= set(topomesh.region_neighbors(0, v))
                topomesh_remove_boundary_vertex(topomesh, v)
                removed_vertices += 1
        print(f"Removed {removed_vertices} vertices! -> {topomesh.nb_wisps(0)} vertices")
        
        vertex_positions = {v:topomesh.wisp_property('barycenter', 0)[v] for v in topomesh.wisps(0)}
        topomesh.update_wisp_property('barycenter', 0, vertex_positions)
        
        property_topomesh_vertices_deformation(topomesh, 
                                           omega_forces={'laplacian_smoothing':0.1}, 
                                           sigma_deformation=target_edge_length/10., 
                                           iterations=10)
        vertex_positions = topomesh.wisp_property('barycenter', 0).to_dict()
        vertex_positions.update(dict(zip(border_vertices, border_points)))
        topomesh.update_wisp_property('barycenter', 0, vertex_positions)

        compute_topomesh_property(topomesh, 'vertices', 1)
        compute_topomesh_property(topomesh, 'length', 1)
        print(f"Average edge length = {topomesh.wisp_property('length', 1).values().mean()}")
        compute_topomesh_vertex_property_from_edges(topomesh, 'length', weighting='uniform')

        inner_vertices = np.array([v for v in topomesh.wisps(0) if v not in border_vertices])
        if len(inner_vertices) > 0:
            inner_vertex_lengths = topomesh.wisp_property('length', 0).values(inner_vertices)
            inner_vertex_length_sorting = np.argsort(inner_vertex_lengths)

            sorted_inner_vertices = inner_vertices[inner_vertex_length_sorting]
            sorted_inner_vertex_lengths = inner_vertex_lengths[inner_vertex_length_sorting]

            vertices_to_remove = sorted_inner_vertices[sorted_inner_vertex_lengths < 3*target_edge_length/4]
        else:
            vertices_to_remove = []
        
        
def wall_mesh_normals(topomesh, wall_cells, cell_centers):
    l, r = wall_cells
    compute_topomesh_property(topomesh, 'borders', 1)
    
    
    border_vertices = np.array([v for v in topomesh.wisps(0) if topomesh.nb_regions(0, v) == 1])
    oriented_vertices = [border_vertices[0]]
    oriented_edges = []
    
    while len(oriented_vertices) < topomesh.nb_wisps(0):
        last_vertex = oriented_vertices[-1]
        vertex_edges = [e for e in topomesh.regions(0, last_vertex) if not e in oriented_edges]
        if len(vertex_edges) == 1:
            next_edge = vertex_edges[0]
            candidate_vertices = [v for v in topomesh.borders(1, next_edge) if not v in oriented_vertices]
            if len(candidate_vertices) == 1:
                next_vertex = candidate_vertices[0]
                oriented_vertices += [next_vertex]
                oriented_edges += [next_edge]
            elif len(candidate_vertices) == 0:
                remaining_border_vertices = [v for v in border_vertices if not v in oriented_vertices]
                if len(remaining_border_vertices)>0:
                    next_vertex = remaining_border_vertices[0]
                    oriented_vertices += [next_vertex]
                else:
                    raise ValueError(f"Wrong border vertices: {remaining_border_vertices}")
            else:
                raise ValueError(f"Wrong vertices: {candidate_vertices}")
        elif len(vertex_edges) == 0:
            remaining_border_vertices = [v for v in border_vertices if not v in oriented_vertices]
            if len(remaining_border_vertices)>0:
                next_vertex = remaining_border_vertices[0]
                oriented_vertices += [next_vertex]
        else:
            raise ValueError(f"Wrong edges: {vertex_edges}")         
    
    edge_vertices = np.transpose([oriented_vertices[:-1], oriented_vertices[1:]])
    edge_vertex_points = topomesh.wisp_property('barycenter', 0).values(edge_vertices)
    edge_vectors = edge_vertex_points[:, 1] - edge_vertex_points[:, 0]
    edge_vectors /= np.linalg.norm(edge_vectors, axis=1)[:, np.newaxis]
    edge_normals = edge_vectors[:, np.array([1, 0, 2])] * np.array([1, -1, 1])
    
    left_to_right_vector = cell_centers[r] - cell_centers[l]
    edge_normal_left_to_right = np.sign(np.dot(edge_normals[:, :2], left_to_right_vector))
    edge_normals = np.median(edge_normal_left_to_right)*edge_normals
    
    topomesh.update_wisp_property('normal', 1, dict(zip(oriented_edges, edge_normals)))
    compute_topomesh_vertex_property_from_edges(topomesh, 'normal', weighting='length')
    
    