# Filament Polarity Analysis


## Authors:
* Guillaume Cerutti (<guillaume.cerutti@ens-lyon.fr>)


## Installation

### Pre-requisite : Install conda

* Open a terminal window and type `conda`. If no error message appear (but a long how-to message) then you have successfully installed `conda`.

* Otherwise, you may choose to install either [the miniconda tool](https://docs.conda.io/en/latest/miniconda.html) or [the anaconda distribution](https://docs.anaconda.com/anaconda/install/) suitable for your OS.

### Download the source repository

#### Using the `git` command line tool

* Open a terminal window and navigate to the directory of your choice using the `cd` command.

* Copy the source repository using `git` by typing:

```
git clone https://gitlab.inria.fr/gcerutti/filament_polarity.git
```

#### Using the download link on Gitlab

* Click on the **Download** link (left to the **Clone** button) on top of this page to download the sources as an archive.

* Decompress the archive in the directory of your choice.

### Create a new conda environment

* In the terminal window, go to the directory where you copied the source repository:

```
cd filament_polarity
```

* Create a new environment containing all the script dependencies using the provided YAML file:

```
conda env create -f environment.yml
```

## Usage

### Preliminaries

* **Activate the conda environment** Each time you open a new terminal window to use a script, you will need to activate the environment you created to access the dependencies

```
conda activate filament-polarity
```

### Executing Notebooks

* **Launch Jupyter to execute notebooks** Jupyter will open in your web browser, where you will be able to access and open the notebooks in the `notebook/` folder.

```
jupyter notebook
```
